import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { User } from './../model/User';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user = new User();
  err = 0;

  constructor(private authService: AuthService,
              public router: Router) { }

  ngOnInit(): void {
  }

  onLoggedin() {
    this.authService.login(this.user).subscribe((data) => {
      console.log('full data :' , data);
      const jwToken = data.token;
      console.log('Token :' , jwToken);
      this.authService.saveToken(jwToken);
      this.router.navigate(['/ocr']);
    }, (err) => {   this.err = 1;
});

 }
}
