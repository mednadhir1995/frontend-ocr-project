import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';
import { User } from './model/User';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  apiURL = 'http://127.0.0.1:5000/ocr';

  username = '';
  token: string;
  public loggedUser: string;
  public isloggedIn = false;
  private helper = new JwtHelperService();
  constructor(private router: Router, private http: HttpClient) { }

  login(user: User): Observable<any> {
  return this.http.post<User>(this.apiURL + '/login' , user );
  }

  saveToken(jwt: string) {
    localStorage.setItem('jwt', jwt);
    this.token = jwt;
    this.isloggedIn = true;
    this.decodeJWT();
  }

  decodeJWT() {
       if (this.token === undefined) {
            return;
   }
       const decodedToken = this.helper.decodeToken(this.token);
       if (this.isloggedIn === true) {
       this.loggedUser = decodedToken.username; } // public_id
  }
  loadToken() {
    this.token = localStorage.getItem('jwt');
    this.decodeJWT();
  }

  getToken(): string {
    return this.token;
  }

  logout() {
    this.loggedUser = undefined;
    this.token = undefined;
    this.isloggedIn = false;
    localStorage.removeItem('jwt');
    this.router.navigate(['/login']);
  }

  isTokenExpired(): boolean  {
    return  this.helper.isTokenExpired(this.token);
  }


  // NEED explanation
  setLoggedUserFromLocalStorage(login: string) {
    this.loggedUser = login;
    this.isloggedIn = true;
    // this.getUserRoles(login);
  }
}
