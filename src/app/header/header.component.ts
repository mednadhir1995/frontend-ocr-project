import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  isLoggedin: boolean ;
  constructor(public authService: AuthService,  private router: Router) {}

ngOnInit() {
  this.authService.loadToken();
  if (this.authService.getToken() === null || this.authService.isTokenExpired()) {
        this.router.navigate(['/login']);
  }
}

onLogout() {
  this.authService.logout();
}
}
