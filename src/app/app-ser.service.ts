import { Injectable } from '@angular/core';
import { HttpClient, HttpParams , HttpHeaders } from '@angular/common/http';
import { Observable} from 'rxjs';
import { map } from 'rxjs/operators';
import { Response} from './interface';
import { AuthService } from './auth.service';



export class LeadModel {
/*   login: string;
  password: string; */
  FirstName: string;
  LastName: string;
  Company: string;
  Email: string;
  Website: string;
  Phone: string;
  Industry: string;

  // tslint:disable-next-line:max-line-length
  constructor( FirstName: string, LastName: string, Company: string, Email: string, Website: string, Phone: string,  Industry: string) {
  /*   this.login = login;
    this.password = password */;
    this.FirstName = FirstName;
    this.LastName = LastName;
    this.Company = Company;
    this.Email = Email;
    this.Website = Website;
    this.Phone = Phone;
    this.Industry = Industry;

  }
}

const httpOptions = {
  headers: new HttpHeaders( {'Content-Type': 'application/json'} )
  };





@Injectable({
  providedIn: 'root'
})
export class AppSerService {

  constructor(private  httpClient: HttpClient, private authService: AuthService) { }

  readonly host = 'http://localhost:5000/ocr/elastic/name';
  readonly host3 = 'http://localhost:5000/ocr/elastic/lastname';
  readonly ocrHost = 'http://localhost:5000/ocr/Upload';
  readonly host2 = 'http://localhost:9200/names/_doc/';
  readonly host4 = 'http://localhost:9200/lastnames/_doc/';
  readonly host5 = 'http://localhost:9200/companynames/_doc/';
  readonly host6 = 'http://localhost:5000/ocr/elastic/company';

  readonly testurl = 'http://localhost:5000/ocr/add';
  public autoComplete(name: string): Observable<Response> {


    const params = new HttpParams()
        .set('name', name);
    return this.httpClient.get<Response>(this.host, { params});
}


public autoCompleteLastname(lastname: string): Observable<Response> {


  const params = new HttpParams()
      .set('lastname', lastname);
  return this.httpClient.get<Response>(this.host3, { params});
}

public autoCompleteCompanyname(company: string): Observable<Response> {


  const params = new HttpParams()
      .set('company', company);
  return this.httpClient.get<Response>(this.host6, { params});
}



addName(firstname: string): Observable<any> {
  return this.httpClient.post(this.host2, {firstname});
}

addLastname(lastname: string): Observable<any> {
  return this.httpClient.post(this.host4, {lastname});
}



addCompanyName(company: string): Observable<any> {

  return this.httpClient.post(this.host5, {company}); }




sendDatatosalesforce(lead: LeadModel ): Observable<any> {
  const  body = { lead};
  const jwt = this.authService.getToken();
  const httpHeaders = new HttpHeaders({'x-access-token': jwt});

  return this.httpClient.post(this.testurl, body, {headers: httpHeaders}) ;

}


imageConverter(body) {
  const jwt = this.authService.getToken();
  const httpHeaders = new HttpHeaders({'x-access-token': jwt});
  return this.httpClient.post(this.ocrHost, body, {headers: httpHeaders});
}


}




